package fr.pluginmakers.hs;

public class OwnedHorse {

	private String owner;
	private int eid;
	private String nick = null;
	
	public OwnedHorse(String owner, int eid, String nick) {
		this.owner = owner;
		this.eid = eid;
		this.nick = nick;
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
}
