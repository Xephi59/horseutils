package fr.pluginmakers.hs;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

public class HorseCmd implements CommandExecutor{

	Main plug;
	
	public HorseCmd(Main plugin){
		plug = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lbl, String[] args) {
		if(!(cs instanceof Player)){
			cs.sendMessage(ChatColor.RED+plug.config.getString("must_be_player"));
			return true;
		}
		
		Player p =(Player)cs;
		if(p.hasPermission("horseutils.call")){
			if(args.length == 0){
				OwnedHorse oh = null;
				int l = 0;
				for(OwnedHorse horse:plug.horses){
					if(horse.getOwner().equals(p.getName())){
						l++;
						oh = horse;
					}
				}
				if(l > 1){
					p.sendMessage(ChatColor.RED+plug.config.getString("more_than_one"));
				}else{
					if(oh != null){
						if(plug.config.getInt("tp_cost") == 0){
							tpHorse2Player(oh, p);
							p.sendMessage(ChatColor.GREEN+plug.config.getString("horse_teleported"));
						}else{
							if(plug.econ.hasAccount(p.getPlayer().getName())){
								if(plug.econ.getBalance(p.getName()) - plug.config.getInt("tp_cost") >=0){
									plug.econ.withdrawPlayer(p.getName(), plug.config.getInt("tp_cost"));
									tpHorse2Player(oh, p);
									p.sendMessage(ChatColor.GREEN+plug.config.getString("horse_teleported"));
								}else{
									p.sendMessage(ChatColor.RED+plug.config.getString("no_enough_money"));
								}
							}else{
								p.sendMessage(ChatColor.RED+plug.config.getString("no_account"));
							}
						}
					}else{
						p.sendMessage(ChatColor.RED+plug.config.getString("no_horse"));
					}
				}
				return true;
			}else if(args.length == 1){
				OwnedHorse horse = null;
				boolean own = false;
				for(OwnedHorse oh:plug.horses){
					if(oh.getOwner().equals(p.getName())){
						own = true;
						if(oh.getNick().equals(args[0])){
							horse = oh;
						}
					}
				}
				if(own){
					if(horse != null){
						if(plug.config.getInt("tp_cost") == 0){
							tpHorse2Player(horse, p);
							p.sendMessage(ChatColor.GREEN+plug.config.getString("horse_teleported"));
						}else{
							if(plug.econ.hasAccount(p.getPlayer().getName())){
								if(plug.econ.getBalance(p.getName()) - plug.config.getInt("tp_cost") >=0){
									plug.econ.withdrawPlayer(p.getName(), plug.config.getInt("tp_cost"));
									tpHorse2Player(horse, p);
									p.sendMessage(ChatColor.GREEN+plug.config.getString("horse_teleported"));
								}else{
									p.sendMessage(ChatColor.RED+plug.config.getString("no_enough_money"));
								}
							}else{
								p.sendMessage(ChatColor.RED+plug.config.getString("no_account"));
							}
						}
					}else{
						p.sendMessage(ChatColor.RED+plug.config.getString("no_horse_named"));
					}
				}else{
					p.sendMessage(ChatColor.RED+plug.config.getString("no_horse"));
				}
				return true;
			}else{
				return false;
			}
		}else{
			p.sendMessage(ChatColor.RED+plug.config.getString("dont_have_perm"));
			return true;
		}
	}

	private void tpHorse2Player(OwnedHorse oh, Player p){
		for(World w:plug.getServer().getWorlds()){
			for(Entity e:w.getEntities()){
				if(e.getEntityId() == oh.getEid()){
					if(e.getType() == EntityType.HORSE){
						e.teleport(p);
						Horse h = (Horse)e;
						h.setPassenger(p);
					}
				}
			}
		}
	}
	
}
