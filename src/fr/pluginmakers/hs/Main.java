package fr.pluginmakers.hs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

	//-------------------------------------------------------------vars
	
	private File configfile;
	public FileConfiguration config;
	private File datafile;
	private FileConfiguration data;
	
	public ArrayList<OwnedHorse> horses = new ArrayList<OwnedHorse>();
	public Economy econ;
	
	private String[] configkeys = {"lock_horses", 
			"lock_horses_deny_message", 
			"lock_horses_claim_message",
			"god_horses",
			"display_owner",
			"display_owner_message",
			"no_horse_spawn",
			"must_be_player",
			"dont_have_perm",
			"must_be_riding",
			"nick_msg",
			"direct_claim",
			"owner_msg",
			"more_than_one",
			"no_horse",
			"tp_cost",
			"horse_teleported",
			"no_account",
			"no_horse_named",
			"nick_price",
			"autotame_price"};
	private Object[] configvalues = {true,
			"This horse is already owned by %owner%",
			"You succesfully owned this horse",
			false,
			true,
			"%player%'s horse",
			false,
			"You must be a player!",
			"You don't have permission!",
			"You must be riding a horse to do that!",
			"You just nicknamed your horse as %nick%",
			false,
			"Owner",
			"You have more than one horses, nickname them and type /horse <nickname>",
			"You don't have any horses",
			0,
			"Your horse has joined you",
			"You don't have any bank account",
			"You don't have any horse with this name",
			0,
			0};
	
	//-------------------------------------------------------------onEnable
	
	public void onEnable(){
		setupConf();
		setupData();
		setupEcon();
		getServer().getPluginManager().registerEvents(new HorseListener(this), this);
		getCommand("nickhorse").setExecutor(new NickHorseCmd(this));
		getCommand("horse").setExecutor(new HorseCmd(this));
	}
	
	//-------------------------------------------------------------onDisable
	
	public void onDisable(){
		saveData();
	}
	
	//-------------------------------------------------------------config
	
	private boolean setupConf(){
		
		if(!getDataFolder().exists()){
			getDataFolder().mkdir();
		}
		configfile = new File(getDataFolder()+File.separator+"config.yml");
		if(!configfile.exists()){
			config = YamlConfiguration.loadConfiguration(configfile);
			verifyConf();
			try {
				config.save(configfile);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		config = YamlConfiguration.loadConfiguration(configfile);
		verifyConf();
		return true;
	}
	
	private void verifyConf(){
		for(int i = 0; i<configkeys.length; i++){
			String key = configkeys[i];
			if(!config.contains(key)){
				config.set(key, configvalues[i]);
			}
		}
		saveConf();
	}
	
	public boolean saveConf(){	
		try {
			config.save(configfile);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	//-------------------------------------------------------------data
	
	public boolean setupData(){
		if(!getDataFolder().exists()){
			getDataFolder().mkdir();
		}
		datafile = new File(getDataFolder()+File.separator+"data.yml");
		if(!datafile.exists()){
			data = YamlConfiguration.loadConfiguration(datafile);
			try {
				data.save(datafile);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		loadData();
		return true;
	}
	
	public void loadData(){
		data = YamlConfiguration.loadConfiguration(datafile);
		for(String entry:data.getKeys(false)){
			String owner = data.getString(entry+".owner");
			int eid = Integer.parseInt(entry);
			String nick = data.getString(entry+".nick");
			horses.add(new OwnedHorse(owner, eid, nick));
		}
		ArrayList<Integer> toremove = new ArrayList<Integer>();
		//TODO
		if(config.getBoolean("display_owner")){
			for(OwnedHorse oh:horses){
				for(World w:Bukkit.getWorlds()){
					for(Entity e:w.getEntities()){
						if(e.getEntityId() == oh.getEid()){
							if(e.getType() == EntityType.HORSE){
								Horse h = (Horse)e;
								if(oh.getNick() != "" || oh.getNick() != null){
									h.setCustomName(ChatColor.AQUA+oh.getNick()+ChatColor.RED+" ("+config.getString("owner_msg")+": "+oh.getOwner()+")");
								}else if(oh.getNick() == "" || oh.getNick() == null){
									h.setCustomName(ChatColor.RED+config.getString("display_owner_message").replace("%player%", oh.getOwner()));
								}
								h.setCustomNameVisible(true);
							}else{
								toremove.add(horses.indexOf(oh));
							}
						}
					}
				}
			}
		}else{
			for(OwnedHorse oh:horses){
				for(World w:Bukkit.getWorlds()){
					for(Entity e:w.getEntities()){
						if(e.getEntityId() == oh.getEid()){
							if(e.getType() == EntityType.HORSE){
								Horse h = (Horse)e;
								h.setCustomName(ChatColor.AQUA+oh.getNick());
							}else{
								toremove.add(horses.indexOf(oh));
							}
						}
					}
				}
			}
		}
		for(int index:toremove){
			horses.remove(index);
		}
	}
	
	public boolean saveData(){	
		datafile.delete();
		data = null;
		data = YamlConfiguration.loadConfiguration(datafile);
		for(OwnedHorse h:horses){
			data.set(h.getEid()+".owner", h.getOwner());
			data.set(h.getEid()+".nick", h.getNick());
		}
		try {
			data.save(datafile);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	//------------------------------------------------------------econ
	
	private boolean setupEcon(){
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            econ = economyProvider.getProvider();
        }

        return (econ != null);
    }
	
}
