package fr.pluginmakers.hs;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class HorseListener implements Listener{

	Main plug;
	
	public HorseListener(Main plugin){
		plug = plugin;
	}
	
	@EventHandler
	public void onPlayerMountHorse(PlayerInteractEntityEvent e){
		if(e.getRightClicked().getType() == EntityType.HORSE){
			Horse h = (Horse)e.getRightClicked();
			String owner = null;
			for(OwnedHorse oh:plug.horses){
				if(oh.getEid() == h.getEntityId()){
					owner = oh.getOwner();
				}
			}
			if(owner != null && plug.config.getBoolean("lock_horses")){
				if(!e.getPlayer().getName().equals(owner)){
					e.getPlayer().sendMessage(ChatColor.RED+plug.config.getString("lock_horses_deny_message").replace("%owner%", owner));
					e.setCancelled(true);
				}
			}else if(owner == null && plug.config.getBoolean("lock_horses")){
				plug.horses.add(new OwnedHorse(e.getPlayer().getName(), h.getEntityId(), ""));
				e.getPlayer().sendMessage(ChatColor.GREEN+plug.config.getString("lock_horses_claim_message"));
				if(plug.config.getBoolean("display_owner")){
					h.setCustomName(ChatColor.RED+plug.config.getString("display_owner_message").replace("%player%", e.getPlayer().getName()));
					h.setCustomNameVisible(true);
				}
				if(plug.config.getBoolean("direct_claim")){
					if(plug.config.getInt("autotame_price") != 0){
						if(plug.econ.hasAccount(e.getPlayer().getName())){
							if(plug.econ.getBalance(e.getPlayer().getName()) - plug.config.getInt("autotame_price") >=0){
								plug.econ.withdrawPlayer(e.getPlayer().getName(), plug.config.getInt("autotame_price"));
								h.setDomestication(h.getMaxDomestication());
							}else{
								e.getPlayer().sendMessage(ChatColor.RED+plug.config.getString("no_enough_money"));
							}
						}else{
							e.getPlayer().sendMessage(ChatColor.RED+plug.config.getString("no_account"));
						}
					}else{
						h.setDomestication(h.getMaxDomestication());
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onHorseDamage(EntityDamageEvent e){
		if(e.getEntity().getType() != EntityType.HORSE){
			return;
		}
		if(plug.config.getBoolean("god_horses")){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onHorseSpawn(CreatureSpawnEvent e){
		if(plug.config.getBoolean("no_horse_spawn")){
			if(e.getEntityType() == EntityType.HORSE){
				if(e.getSpawnReason() != SpawnReason.EGG){//only eggs allowed
					e.setCancelled(true);
				}
			}
		}
	}
	
}
