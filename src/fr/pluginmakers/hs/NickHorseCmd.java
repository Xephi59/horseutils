package fr.pluginmakers.hs;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

public class NickHorseCmd implements CommandExecutor{

	Main plug;
	
	public NickHorseCmd(Main plugin){
		plug = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lbl, String[] args) {
		if(args.length != 1){
			return false;
		}
		if(!(cs instanceof Player)){
			cs.sendMessage(ChatColor.RED+plug.config.getString("must_be_player"));
			return true;
		}
		Player p = (Player)cs;
		if(!p.hasPermission("horseutils.nick")){
			p.sendMessage(ChatColor.RED+plug.config.getString("dont_have_perm"));
			return true;
		}
		if(p.isInsideVehicle()){
			Entity e = p.getVehicle();
			if(e.getType() == EntityType.HORSE){
				Horse h = (Horse)e;
				for(OwnedHorse oh:plug.horses){
					if(h.getEntityId() == oh.getEid()){
						if(plug.config.getInt("nick_price") != 0){
							if(plug.econ.hasAccount(p.getName())){
								if(plug.econ.getBalance(p.getName()) - plug.config.getInt("nick_price") >= 0){
									oh.setNick(args[0]);
									if(plug.config.getBoolean("display_owner")){
										h.setCustomName(ChatColor.AQUA+oh.getNick()+ChatColor.RED+" ("+plug.config.getString("owner_msg")+": "+oh.getOwner()+")");
									}else{
										h.setCustomName(ChatColor.AQUA+oh.getNick());
									}
									p.sendMessage(ChatColor.GREEN+plug.config.getString("nick_msg").replace("%nick%", oh.getNick()));
								}else{
									p.sendMessage(ChatColor.RED+plug.config.getString("no_enough_money"));
								}
							}else{
								p.sendMessage(ChatColor.RED+plug.config.getString("no_account"));
							}
						}else{
							oh.setNick(args[0]);
							if(plug.config.getBoolean("display_owner")){
								h.setCustomName(ChatColor.AQUA+oh.getNick()+ChatColor.RED+" ("+plug.config.getString("owner_msg")+": "+oh.getOwner()+")");
							}else{
								h.setCustomName(ChatColor.AQUA+oh.getNick());
							}
							p.sendMessage(ChatColor.GREEN+plug.config.getString("nick_msg").replace("%nick%", oh.getNick()));
						}
					}
				}
				return true;
			}else{
				p.sendMessage(ChatColor.RED+plug.config.getString("must_be_riding"));
				return true;
			}
		}else{
			p.sendMessage(ChatColor.RED+plug.config.getString("must_be_riding"));
			return true;
		}
	}
	
}
